﻿using Newtonsoft.Json;
using SendAttachmentBot.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace SendAttachmentBot.Services
{
    public class OrderService
    {
        public static async Task<Item> GetItem(string name)
        {
            string uri = $"https://hooks.slack.com/services/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";
            var item = new Item();
            using (var client = new WebClient())
            {
                var rawData = await client.DownloadStringTaskAsync(new Uri(uri));
                item = JsonConvert.DeserializeObject<Item>(rawData);                
            }
            return item;
        }
    }
}