﻿namespace SendAttachmentBot
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using System.Web;
    using Microsoft.Bot.Builder.Dialogs;
    using Microsoft.Bot.Connector;

    [Serializable]
    internal class SendFoodItem : IDialog<object>
    {
        private const string burgar = "(1) Burgar";
        private const string chaumin = "(2) Chaumin";
        private const string momo = "(3) MOMO";
        private const string pizza = "(4) Pizza";
        private const string samosa = "(5) Samosa";
        private readonly IDictionary<string, string> options = new Dictionary<string, string>
        {
            { "1", burgar },
            { "2", chaumin },
            { "3", momo },
            { "4", pizza },
            { "5", samosa }
        };

        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(this.MessageReceivedAsync);
        }

        public async virtual Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;

            var welcomeMessage = context.MakeMessage();
            welcomeMessage.Text = "Welcome, here you can see attachment alternatives:";

            await context.PostAsync(welcomeMessage);

            await this.DisplayOptionsAsync(context);
        }

        public async Task DisplayOptionsAsync(IDialogContext context)
        {
            PromptDialog.Choice<string>(
                context,
                this.ProcessSelectedOptionAsync,
                this.options.Keys,
                "What would you like for lunch today ?",
                "Ooops, what you wrote is not a valid option, please try again",
                3,
                PromptStyle.PerLine,
                this.options.Values);
        }

        public async Task ProcessSelectedOptionAsync(IDialogContext context, IAwaitable<string> argument)
        {
            var message = await argument;

            var replyMessage = context.MakeMessage();

            Attachment attachment = null;

            switch (message)
            {
                case "1":
                    attachment = GetBurgarAttachment();
                    break;
                case "2":
                    attachment = GetChauminAttachment();
                    break;
                case "3":
                    attachment = GetMomoAttachment();
                    break;
                case "4":
                    attachment = GetPizzaAttachment();
                    break;
                case "5":
                    attachment = GetSamosaAttachment();
                    break;
            }

            // The Attachments property allows you to send and receive images and other content
            replyMessage.Attachments = new List<Attachment> { attachment };

            await context.PostAsync(replyMessage);

            await this.DisplayOptionsAsync(context);
        }

        private static Attachment GetBurgarAttachment()
        {
            var imagePath = HttpContext.Current.Server.MapPath("~/images/burgar.jpg");

            var imageData = Convert.ToBase64String(File.ReadAllBytes(imagePath));

            return new Attachment
            {
                Name = "burgar.jpg",
                ContentType = "image/jpg",
                ContentUrl = $"data:image/jpg;base64,{imageData}"
            };
        }

        private static Attachment GetChauminAttachment()
        {
            var imagePath = HttpContext.Current.Server.MapPath("~/images/chaumin.jpg");

            var imageData = Convert.ToBase64String(File.ReadAllBytes(imagePath));

            return new Attachment
            {
                Name = "chaumin.jpg",
                ContentType = "image/jpg",
                ContentUrl = $"data:image/jpg;base64,{imageData}"
            };
        }

        private static Attachment GetMomoAttachment()
        {
            var imagePath = HttpContext.Current.Server.MapPath("~/images/momo.jpg");

            var imageData = Convert.ToBase64String(File.ReadAllBytes(imagePath));

            return new Attachment
            {
                Name = "momo.jpg",
                ContentType = "image/jpg",
                ContentUrl = $"data:image/jpg;base64,{imageData}"
            };
        }
        private static Attachment GetPizzaAttachment()
        {
            var imagePath = HttpContext.Current.Server.MapPath("~/images/pizza.jpg");

            var imageData = Convert.ToBase64String(File.ReadAllBytes(imagePath));

            return new Attachment
            {
                Name = "pizza.jpg",
                ContentType = "image/jpg",
                ContentUrl = $"data:image/jpg;base64,{imageData}"
            };
        }
        private static Attachment GetSamosaAttachment()
        {
            var imagePath = HttpContext.Current.Server.MapPath("~/images/samosa.jpg");

            var imageData = Convert.ToBase64String(File.ReadAllBytes(imagePath));

            return new Attachment
            {
                Name = "samosa.jpg",
                ContentType = "image/jpg",
                ContentUrl = $"data:image/jpg;base64,{imageData}"
            };
        }
    }
}
