﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SendAttachmentBot.Model
{
    public class Item
    {
        public int Id { get; set; }
        public string ItemName { get; set; }
    }
}